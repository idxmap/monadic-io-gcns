#!/bin/bash

SRCDIR="../asm_compose_SRCDIR/$2"
DSTDIR="../asm_compose_DSTDIR/$2"

echo "[| run.sh |] <<< Running julia (which calls \`cargo run\`) ..."

time julia load.jl $1 "$SRCDIR/$3" novisual "$DSTDIR"

rm "$SRCDIR/$3.generated.ASM_COMPOSE_GRAPH.mp"

# mv "$SRCDIR/$3.generated.ASM_COMPOSE_GRAPH.mp.FINAL.jld2" "$DSTDIR/"


echo "[| run.sh |] <<< Done."
