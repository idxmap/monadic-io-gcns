defmodule ExtractEXEs do
    use GenServer

    def start_link do
        GenServer.start_link(__MODULE__, :ok)
    end

    def start_link(opts) do
        GenServer.start_link(__MODULE__, :ok, opts)
    end

    @impl true
    def init(:ok) do
        {:ok, nil}
    end

    @impl true
    def handle_cast({:proc, dt_path, [from: from]}, state) do
        next_callback =
            fn -> send(from, {:nextitem, [from: self()]}) end
        process_winget_data(dt_path, next_callback)
        {:noreply, state}
    end

    defp process_winget_data(dt_path, next_callback) do
        with {:ok, fcontents} <- File.read(dt_path)
        do
            url_prefix = "InstallerUrl: "
            fcontents
                |> String.split("\n", trim: true)
                |> Enum.map(
                    &String.trim/1
                )
                |> Enum.filter(
                    &String.starts_with?(&1, url_prefix)
                )
                |> Enum.map(
                    &String.slice(
                        &1,
                        ( String.length url_prefix )..( String.length &1 )
                    )
                )
                |> Enum.filter(
                    &String.ends_with?(&1, ".exe")
                )
                |> Enum.each(
                    &process_resource_url/1
                )
            next_callback.()
        else
            {:error, :ENOENT} -> {:err, :enoent}
            _ -> {:err, :unknown}
        end
    end

    defp process_resource_url(urlstr) do
        output_dir = "curl_save/"
        System.cmd(
            "curl",
            ["-o", output_dir <> uniq_outfile() <> ".exe", urlstr]
        )
    end

    defp uniq_outfile do
        "R_at_#{inspect self()}_t_#{to_string System.monotonic_time}"
    end
end
