import r2pipe
import json
import os
from os import listdir
from os.path import isfile, join
import sys

def proc1(fname, refertoas):
    r2 = r2pipe.open(fname)
    si = r2.cmd("iSj")
    res = json.loads(si)
    execsections = [ x for x in res if 'x' in x["perm"] ]
    os.system(f"touch {refertoas}.gpartINIT")
    os.system(f"touch {refertoas}.gpartNNVL")
    for i in execsections:
        r2.cmd(f"s {i['vaddr']}")
        r2.cmd(f"pd {i['vsize']} >> {refertoas}.gpartINIT")
    os.system(f"sh -c 'cat {refertoas}.gpartINIT | grep -v invalid > {refertoas}.gpartNNVL'")
    os.system(f"mv {refertoas}.gpartNNVL {refertoas}")
    os.system(f"rm {refertoas}.gpartINIT")

if __name__ == "__main__":
    mypath = f"EXESAMPLES_{sys.argv[1]}"
    os.system(f"mkdir EXEASM_{sys.argv[1]}/")
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    for fname in onlyfiles:
        proc1(join(mypath, fname), f"EXEASM_{sys.argv[1]}/{fname}.r2asm")
        os.system(f"./rminvalid.sh {fname}.*.r2asm")
        os.system(f"rm -rf {fname}.*.gPARTINIT")
        os.system(f"rm -rf {fname}.*.gPARTNNVL")
