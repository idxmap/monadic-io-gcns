/*
    asm_compose: Representing Assembly with Monadic IO
*/

#![allow(unused_variables)]
#![allow(unused_imports)]
#![allow(dead_code)]

#![forbid(unsafe_code)]

use std::collections::{HashMap, HashSet};

/// file(`Cargo.toml`) includes dep(`petgraph`)
use petgraph::{graph, graph::Graph};

use std::env;

#[macro_use]
extern crate lazy_static;

/// target(build) only
fn main() -> Result<(), std::io::Error> {
    let args = env::args().collect::<Vec<_>>();
    let disasm_fname = &args[1];
    println!("[INFO] #[MAIN] Processing disasm file {:?}...", disasm_fname);
    process_file(&disasm_fname)?;
    println!("[INFO] #[MAIN] ...Done!");
    Ok(())
}

use export::GraphingTarget;

fn process_file(fname: &str) -> Result<(), std::io::Error> {
    let gtarget = export::RadareGraphingTarget {
        disasm_fname: fname,
    };
    let outfile = fname.to_owned() + ".generated.ASM_COMPOSE_GRAPH.mp";
    gtarget.graph_to_file(&outfile)?;
    Ok(())
}

mod export {
    use serde_derive;
    use rmp_serde;

    // use regex::Regex;

    use serde_derive::{Serialize, Deserialize};

    use std::fs::OpenOptions;
    use std::io::Write;

    use petgraph::visit::EdgeRef;

    type ResultingGraph<Y> = petgraph::graph::Graph<
        <Y as GraphingTarget>::N,
        <Y as GraphingTarget>::E,
        <Y as GraphingTarget>::Dir,
        <Y as GraphingTarget>::Ix,
    >;

    trait IsDirected {}

    impl IsDirected for petgraph::Directed {}

    pub trait GraphingTarget {
        type N;
        type E;
        type Dir;
        type Ix: petgraph::graph::IndexType;

        type Err;

        fn graph(&self) -> Result<ResultingGraph<Self>, <Self as GraphingTarget>::Err>;

        fn graph_to_file(&self, outfile: &str) -> Result<(), std::io::Error>;
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    struct GraphWalkableAdjList<N>(Vec<(usize, N, Vec<usize>)>);

    impl<N: Copy> GraphWalkableAdjList<N> {
        fn from_graph<Ix: petgraph::graph::IndexType>(
            g: petgraph::graph::Graph<N, (), petgraph::Directed, Ix>
        ) -> Self {
            Self(
                g
                    .node_indices()
                    .into_iter()
                    .map(
                        |x| (
                            x.index(),
                            *g.node_weight(x).unwrap(),
                            {
                                g
                                    .edges(x)
                                    .map(|y| y.target().index())
                                    .collect::<Vec<_>>()
                            },
                        )
                    )
                    .collect::<Vec<_>>()
            )
        }
    }

    pub struct RadareGraphingTarget<'a> {
        pub disasm_fname: &'a str,
    }

    impl GraphingTarget for RadareGraphingTarget<'_> {
        type N = crate::Node;
        type E = ();
        type Dir = petgraph::Directed;
        type Ix = petgraph::graph::DefaultIx;

        type Err = std::io::Error;

        fn graph(&self) -> Result<ResultingGraph<Self>, <Self as GraphingTarget>::Err> {
            let mut line_iter = crate::LineByLine::new(
                self.disasm_fname,
                Box::new(
                    |s| {
                        crate::radare_disasm::parse_line(&s)
                            .unwrap_or(crate::disasm_repr::DisassemblyLine::Garbage)
                    },
                ),
            )?;
            Ok(crate::build_graph(&mut line_iter))
        }

        fn graph_to_file(&self, outfile: &str) -> Result<(), std::io::Error> {
            let g = self.graph()?;
            /*println!(
                "RESULT := {:?}",
                petgraph::dot::Dot::with_config(
                    &g,
                    &[petgraph::dot::Config::EdgeNoLabel]
                ),
            );*/
            let walkable_adj_list = GraphWalkableAdjList::from_graph(g);
            let buf = rmp_serde::to_vec(&walkable_adj_list).unwrap();
            let mut fh = {
                OpenOptions::new()
                    .create(true)
                    .write(true)
                    .open(outfile)?
            };
            fh.write_all(&buf)?;
            Ok(())
        }
    }
}

mod radare_disasm {
    use std::str::FromStr;

    use regex::Regex;

    impl FromStr for crate::disasm_repr::DisassemblyLine {
        type Err = std::io::Error;

        fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
            let s = s.replace(|c: char| !c.is_ascii(), "?");
            let s = s.trim();
            let aligned_ix_zero = s.find('0');
            let aligned_ix_sc = s.find(';');
            let comment_only: bool; // これらはラダレからです
            let aligned_ix: usize;
            if let Some(zero_ix) = aligned_ix_zero {
                if let Some(sc_ix) = aligned_ix_sc {
                    if sc_ix < zero_ix {
                        comment_only = true;
                        aligned_ix = sc_ix;
                    } else {
                        comment_only = false;
                        aligned_ix = zero_ix;
                    }
                } else {
                    comment_only = false;
                    aligned_ix = zero_ix;
                }
            } else {
                if let Some(sc_ix) = aligned_ix_sc {
                    comment_only = true;
                    aligned_ix = sc_ix;
                } else {
                    return Ok(
                        crate::disasm_repr::DisassemblyLine::Garbage
                    );
                }
            }
            if comment_only {
                if *(&s[aligned_ix..].starts_with(";-- ")) {
                    let mut fun_name_owned = (&s[(aligned_ix + 4)..]).to_owned();
                    fun_name_owned.pop();
                    // println!("手順です、今それはコメントを読めます。");
                    // dbg!(&fun_name_owned);
                    return Ok(
                        crate::disasm_repr::DisassemblyLine::NowInFunction(
                            crate::disasm_repr::AsmFun::new(fun_name_owned)
                        )
                    );
                } else {
                    // println!("廃品、セミコロン文字の後の役に立たない部分。");
                    return Ok(
                        crate::disasm_repr::DisassemblyLine::Garbage
                    );
                }
            } else {
                let is_join = {
                    if aligned_ix <= 1 {
                        false
                    } else {
                        s.chars().nth(aligned_ix - 2) == Some('>')
                    }
                };
                let disasm_part = &s[aligned_ix..];
                let position_literal = &disasm_part[2..10];
                let asm_part = &s[(aligned_ix + 31)..];
                let instr: crate::disasm_repr::AsmInstr;
                let open_paren_ix = asm_part.find('(');
                let view = open_paren_ix.and_then(
                    |x: usize| (&asm_part[..x]).split_whitespace().last()
                );
                lazy_static! {
                    static ref FUN_LIKE_RE: Regex = {
                        Regex::new("^[A-Z][a-z][A-Za-z0-9_]{3,}$")
                            .unwrap()
                    };
                }
                if {
                    true
                    && open_paren_ix.is_some()
                    // これが必要ありません && view.is_some()
                    && !asm_part.contains(" .string")
                    && view.map(|x: &str| FUN_LIKE_RE.is_match(x)).unwrap_or(false)
                } {
                    let open_paren_ix = open_paren_ix.unwrap();
                    let view = view.unwrap();
                    // dbg!(view);
                    if asm_part.contains(".dll_") || asm_part.contains("sys.") {
                        instr = crate::disasm_repr::AsmInstr::ExtCall(
                            crate::disasm_repr::AsmExtern::new(&view),
                        );
                    } else {
                        instr = crate::disasm_repr::AsmInstr::FunCall(
                            crate::disasm_repr::AsmFun::new(view.to_owned()),
                        );
                    }
                }
                else if asm_part.starts_with("j") {
                    // これは跳躍部分です
                    let hex_to: &str = {
                        if let Some(sc_ix) = aligned_ix_sc {
                            &s[(sc_ix + 2)..] // スペース文字とゼロと「X」も取り外す前に
                        } else {
                            if let Some(next_zero_ix) = asm_part.find('0') {
                                &asm_part[(next_zero_ix + 2)..] // ここにはスペース文字はありません
                            } else {
                                // println!("廃品です、この跳躍は読めません。");
                                return Ok(
                                    crate::disasm_repr::DisassemblyLine::Garbage
                                );
                            }
                        }
                    };
                    if let Ok(position_raw) = u64::from_str_radix_lenient(hex_to, 16u32) {
                        instr = crate::disasm_repr::AsmInstr::Jump(
                            crate::disasm_repr::AsmPosition::new(position_raw),
                            asm_part.starts_with("jmp"),
                        );
                    } else {
                        // println!("廃品です、この跳躍は読めません、おそらく複雑です。");
                        return Ok(
                            crate::disasm_repr::DisassemblyLine::Garbage
                        );
                    }
                }
                else if (&disasm_part[16..]).starts_with(".dword") {
                    if let Some(sc_ix) = aligned_ix_sc {
                        let comment_part = &s[(sc_ix + 1)..];
                        if comment_part.contains(".dll_") {
                            instr = crate::disasm_repr::AsmInstr::ExtCall(
                                crate::disasm_repr::AsmExtern::new(comment_part),
                            );
                        } else {
                            instr = crate::disasm_repr::AsmInstr::Comp(
                                crate::disasm_repr::SimpleInstruction::Unknown
                            );
                        }
                    } else {
                        instr = crate::disasm_repr::AsmInstr::Comp(
                            crate::disasm_repr::SimpleInstruction::Unknown
                        );
                    }
                }
                else if asm_part.starts_with("push str.") {
                    if let Some(sc_ix2) = asm_part.find(';') {
                        let name_part = asm_part[9..(sc_ix2 - 1)].trim();
                        if FUN_LIKE_RE.is_match(name_part) {
                            instr = crate::disasm_repr::AsmInstr::FunCall(
                                crate::disasm_repr::AsmFun::new(
                                    name_part.to_owned()
                                ),
                            );
                        } else {
                            instr = crate::disasm_repr::AsmInstr::Comp(
                                crate::disasm_repr::SimpleInstruction::Unknown
                            );
                        }
                    } else {
                        instr = crate::disasm_repr::AsmInstr::Comp(
                            crate::disasm_repr::SimpleInstruction::Unknown
                        );
                    }
                }
                else if asm_part.starts_with("call") {
                    if let Some(sc_ix) = aligned_ix_sc {
                        let comment_part = {
                            (&s[(sc_ix + 1)..])
                                .trim()
                        };
                        let ws_index: Option<usize> = {
                            if let Some(call_part_index) = comment_part.find(';') {
                                (&comment_part[call_part_index..])
                                    .find(' ')
                            } else {
                                comment_part
                                    .find(' ')
                            }
                        };
                        let mut comment_part: &str = &(s.clone());
                        while let Some(sc_ix_prime) = comment_part.find(';') {
                            comment_part = {
                                (&comment_part[(sc_ix_prime + 1)..])
                                    .trim()
                            };
                        }
                        let begin_args_index = comment_part.find('(');
                        if let Some(ws_index) = ws_index {
                            let fun_name: &str;
                            if let Some(begin_args_index) = begin_args_index {
                                fun_name = &comment_part[(ws_index - 1)..begin_args_index];
                            } else {
                                fun_name = &comment_part[(ws_index - 1)..];
                            }
                            if asm_part.contains(".dll_") {
                                instr = crate::disasm_repr::AsmInstr::ExtCall(
                                    crate::disasm_repr::AsmExtern::new(fun_name),
                                );
                            } else {
                                instr = crate::disasm_repr::AsmInstr::FunCall(
                                    crate::disasm_repr::AsmFun::new(
                                        fun_name.to_owned()
                                    ),
                                );
                            }
                        } else {
                            instr = crate::disasm_repr::AsmInstr::Comp(
                                crate::disasm_repr::SimpleInstruction::Unknown
                            );
                        }
                    } else {
                        instr = crate::disasm_repr::AsmInstr::Comp(
                            crate::disasm_repr::SimpleInstruction::Unknown
                        );
                    }
                }
                else {
                    instr = crate::disasm_repr::AsmInstr::Comp(
                        crate::disasm_repr::SimpleInstruction::Unknown
                    );
                }
                return Ok(
                    crate::disasm_repr::DisassemblyLine::Assembly(
                        crate::disasm_repr::AsmLine {
                            instr,
                            targeted: is_join,
                            position: crate::disasm_repr::AsmPosition::new(
                                u64::from_str_radix_lenient(position_literal, 16u32).unwrap(),
                            ),
                        }
                    )
                );
            }
        }
    }

    trait FromStrRadixLenient where Self: Sized {
        type Err;

        fn from_str_radix_lenient(s: &str, radix: u32) -> Result<Self, <Self as FromStrRadixLenient>::Err>;
    }

    impl FromStrRadixLenient for u64 {
        type Err = std::num::ParseIntError;

        fn from_str_radix_lenient(s: &str, radix: u32) -> Result<Self, <Self as FromStrRadixLenient>::Err> {
            if radix == 16u32 && s.starts_with("0x") {
                u64::from_str_radix(&s[2..], radix)
            } else {
                u64::from_str_radix(s, radix)
            }
        }
    }

    type LineParsingResult = Result<
        crate::disasm_repr::DisassemblyLine,
        <crate::disasm_repr::DisassemblyLine as FromStr>::Err,
    >;

    use std::panic;

    pub fn parse_line(s: &str) -> LineParsingResult {
        let a = panic::catch_unwind(
            move || s.parse::<crate::disasm_repr::DisassemblyLine>().unwrap()
        ).map_err(
            |_| std::io::Error::new(std::io::ErrorKind::InvalidData, "Failure inside parse (or it panicked)!!")
        );
        if a.is_err() { println!("実際の廃品です！"); }
        a
    }
}

pub trait Reset {
    fn reset(&mut self);
}

mod reader_noalloc {
    use std::fs::File;
    use std::io;
    use std::io::prelude::*;

    #[derive(Debug)]
    pub struct BufReader {
        reader: io::BufReader<File>,
    }

    impl BufReader {
        pub fn open(path: impl AsRef<std::path::Path>) -> io::Result<Self> {
            let file = File::open(path)?;
            let reader = io::BufReader::new(file);
            Ok(
                Self { reader }
            )
        }

        pub fn read_line<'a>(
            &mut self,
            buffer: &'a mut String,
        ) -> Option<&'a mut String> {
            buffer.clear();
            self.reader
                .read_line(buffer)
                .map(|u| if u == 0 { None } else { Some(buffer) })
                .unwrap_or(None)
        }

        pub fn get_position_bytes(&mut self) -> u64 {
            self.reader.seek(io::SeekFrom::Current(0i64)).unwrap()
        }

        pub fn seek_to(&mut self, x: io::SeekFrom) -> Result<u64, io::Error> {
            self.reader.seek(x)
        }
    }
}

struct LineByLine<A> {
    reader: reader_noalloc::BufReader,
    func: Box<dyn Fn(String) -> A>,
}

impl<A> LineByLine<A> {
    fn new(
        path: impl AsRef<std::path::Path>,
        func: Box<dyn Fn(String) -> A>
    ) -> Result<Self, std::io::Error> {
        Ok(
            Self {
                reader: reader_noalloc::BufReader::open(path)?,
                func,
            }
        )
    }
}

impl<A> Iterator for LineByLine<A> {
    type Item = A;

    fn next(&mut self) -> Option<Self::Item> {
        let mut owned_buffer = String::new();
        let line = self.reader.read_line(&mut owned_buffer);
        line.map(
            |i| (self.func)(i.to_string()) // クロージェルを使う
        )
    }
}

impl<A> Reset for LineByLine<A> {
    fn reset(&mut self) {
        self.reader.seek_to(std::io::SeekFrom::Start(0u64)).unwrap();
    }
}

use build_graph_m::build_graph;

mod build_graph_m {
    use crate::{
        disasm_repr::*,
        Node,
        Reset,
    };

    use petgraph::graph::*;

    use petgraph::visit::EdgeRef;

    use std::collections::{HashMap, HashSet, VecDeque};

    pub fn build_graph<Ix: IndexType>(
        lines: &mut (impl Iterator<Item = DisassemblyLine> + Reset),
    ) -> Graph<Node, (), petgraph::Directed, Ix> {
        let mut live = Graph::<Node, (), petgraph::Directed, Ix>::default();
        let return_node = live.add_node(Node::TopReturnNode);
        let mut curr_view = Vec::<AsmLine>::new();
        let mut curr_fn = AsmFun::new("#?".to_owned());
        let fun_nodes: HashMap<AsmFun, NodeIndex<Ix>> = {
            lines
                .filter_map(
                    |i: DisassemblyLine| match i {
                        DisassemblyLine::NowInFunction(asm_fun) => {
                            if !asm_fun.0.starts_with("str.") {
                                Some((asm_fun, live.add_node(Node::LiftedPhiNode)))
                            } else {
                                None
                            }
                        },
                        _ => None,
                    }
                )
                .collect::<HashMap<_, _>>()
        };
        lines.reset();
        let extern_nodes: HashMap<AsmExtern, NodeIndex<Ix>> = {
            lines
            .filter_map(
                |i: DisassemblyLine| match i {
                    DisassemblyLine::Assembly(
                        AsmLine {
                            instr: AsmInstr::ExtCall(r#extern),
                            ..
                        }
                    ) => {
                        Some((r#extern, live.add_node(Node::BaseNode(r#extern))))
                    },
                    _ => None,
                }
            )
            .collect::<HashMap<_, _>>()
        };
        lines.reset();
        // let mut run_position = AsmPosition::new(0u64);
        for line in lines {
            match line {
                DisassemblyLine::Garbage => {},
                DisassemblyLine::Assembly(asm_line) => {
                    match &asm_line.instr {
                        AsmInstr::ExtCall(..) => {
                            // println!("observed: AsmInstr::ExtCall(..) !!");
                        },
                        _ => {},
                    }
                    // run_position = asm_line.position;
                    curr_view.push(asm_line);
                },
                DisassemblyLine::NowInFunction(asm_fun) => {
                    if asm_fun.0.starts_with("str.") {
                        continue;
                    }
                    // println!("NowInFunction(AsmFun( \"{}\" ))", asm_fun.0);
                    if !curr_fn.is_init_unknown() {
                        if !curr_view.is_empty() {
                            graph_function_on(
                                &mut live,
                                curr_view,
                                curr_fn.clone(),
                                &fun_nodes,
                                &extern_nodes,
                                return_node,
                            );
                        }
                    }
                    curr_fn = asm_fun;
                    curr_view = Vec::<AsmLine>::new();
                },
            }
        }
        // println!("loop 1 ended");
        // dbg!(&curr_view);
        graph_function_on(
            &mut live,
            curr_view,
            curr_fn,
            &fun_nodes,
            &extern_nodes,
            return_node,
        );
        live
    }

    struct ForkInfo<Ix: IndexType> {
        pos: AsmPosition,
        ix: NodeIndex<Ix>,
        is_cond: bool,
    }

    fn graph_function_on<Ix: IndexType>(
        live: &mut Graph<Node, (), petgraph::Directed, Ix>,
        curr_view: Vec<AsmLine>,
        curr_fn: AsmFun,
        fun_nodes: &HashMap<AsmFun, NodeIndex<Ix>>,
        extern_nodes: &HashMap<AsmExtern, NodeIndex<Ix>>,
        return_node: NodeIndex<Ix>,
    ) -> () {
        let joins: HashSet<AsmPosition> = view_joins(&curr_view);
        if curr_view.len() == 0usize {
            return;
        }
        if let Some(res_node) = fun_nodes.get(&curr_fn) {
            let mut fork_info = HashMap::<AsmPosition, ForkInfo<Ix>>::new();
            let mut join_info = HashMap::<AsmPosition, NodeIndex<Ix>>::new();
            let mut cref: NodeIndex<Ix> = return_node;
            print!("[INFO] Building complete path...");
            for i in curr_view {
                if joins.contains(&i.position) {
                    join_info.insert(i.position, cref.clone());
                }
                match &i.instr {
                    AsmInstr::Jump(pos, is_cond) => {
                        print!(":");
                        fork_info.insert(
                            i.position,
                            ForkInfo {
                                pos: *pos,
                                is_cond: *is_cond,
                                ix: cref.clone(),
                            }
                        );
                    },
                    AsmInstr::FunCall(asm_fun) => {
                        print!("$");
                        if let Some(active_node) = fun_nodes.get(&asm_fun) {
                            let bind = live.add_node(Node::CompNode);
                            live.add_edge(cref, bind, ());
                            live.add_edge(*active_node, bind, ());
                            cref = bind;
                        }
                    },
                    AsmInstr::ExtCall(r#extern) => {
                        print!("!");
                        if let Some(active_node) = extern_nodes.get(&r#extern) {
                            let bind = live.add_node(Node::CompNode);
                            live.add_edge(cref, bind, ());
                            live.add_edge(*active_node, bind, ());
                            cref = bind;
                        }
                    }
                    _ => {},
                }
            }
            println!("...done!");
            print!("[INFO] Removing unconditionally unused edges...");
            for (pos_origin, info) in &fork_info {
                let srcnode = info.ix;
                if join_info.get(&info.pos).is_some() && !info.is_cond {
                    let edges_rm: Vec<EdgeIndex<Ix>> = {
                        live
                            .edges_directed(srcnode, petgraph::Direction::Outgoing)
                            .map(|e| e.id())
                            .collect::<Vec<_>>()
                    };
                    for e in edges_rm {
                        live.remove_edge(e);
                    }
                }
            }
            println!("done!");
            print!("[INFO] Connecting jumps...");
            for (pos_origin, info) in fork_info {
                let srcnode = info.ix;
                if let Some(dstnode) = join_info.get(&info.pos) {
                    live.add_edge(srcnode, *dstnode, ());
                }
            }
            println!("done!");
            /*
            let mut cursors_deque = VecDeque::<(AsmPosition, NodeIndex<Ix>)>::with_capacity(100usize);
            cursors_deque.push_back(
                (
                    curr_view[0usize].position,
                    return_node
                )
            );
            let mut realized_joins = HashMap::<AsmPosition, NodeIndex<Ix>>::new();
            let mut already_visited = HashSet::<AsmPosition>::new();
            while !cursors_deque.is_empty() {
                while let Some(mut cursor) = cursors_deque.pop_front() {
                    // println!("@@ cursor: {:?}", cursor);
                    let mut hit_break = false;
                    let (tpos, n_ix) = cursor;
                    for i in &curr_view {
                        dbg!(cursors_deque.len());
                        if i.position < tpos {
                            // print!("~");
                            continue;
                        }
                        if let Some(join_lifted_phi_node) = realized_joins.get(&i.position) {
                            live.add_edge(cursor.1, *join_lifted_phi_node, ());
                            hit_break = true;
                            break;
                        }
                        if joins.contains(&i.position) {
                            let lifted_phi_node = live.add_node(Node::LiftedPhiNode);
                            realized_joins.insert(i.position, lifted_phi_node);
                            cursors_deque.push_back((i.position, lifted_phi_node));
                            hit_break = true;
                            break;
                        }
                        if already_visited.contains(&i.position) {
                            hit_break = true;
                            break;
                        }
                        already_visited.insert(i.position);
                        cursor.0 = i.position;
                        println!("HIT ref i field instr match");
                        match &i.instr {
                            AsmInstr::Jump(target, false) => {
                                cursors_deque.push_back((*target, n_ix));
                                cursors_deque.push_back(cursor);
                            },
                            AsmInstr::Jump(target, true) => {
                                cursors_deque.push_back((*target, cursor.1));
                            },
                            AsmInstr::FunCall(asm_fun) => {
                                println!("got: fun");
                                if let Some(active_node) = fun_nodes.get(&asm_fun) {
                                    let bind = live.add_node(Node::CompNode);
                                    live.add_edge(cursor.1, bind, ());
                                    live.add_edge(*active_node, bind, ());
                                    cursor.1 = bind;
                                }
                            },
                            AsmInstr::ExtCall(r#extern) => {
                                println!("got: ext");
                                if let Some(active_node) = extern_nodes.get(&r#extern) {
                                    let bind = live.add_node(Node::CompNode);
                                    live.add_edge(cursor.1, bind, ());
                                    live.add_edge(*active_node, bind, ());
                                    cursor.1 = bind;
                                }
                            },
                            _ => {},
                        }
                    }
                    if !hit_break {
                        // println!("got: res");
                        live.add_edge(cursor.1, *res_node, ());
                    }
                }
            }
            */
        }
    }

    fn view_joins(view: &Vec<AsmLine>) -> HashSet<AsmPosition> {
        let mut acc = HashSet::<AsmPosition>::new();
        for asm_line in view {
            if asm_line.targeted {
                acc.insert(asm_line.position);
            }
            match asm_line.instr {
                AsmInstr::Jump(target, ..) => {
                    acc.insert(target);
                },
                _ => {},
            }
        }
        acc
    }
}

use serde_derive::{Serialize, Deserialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum Node {
    CompNode,
    BaseNode(disasm_repr::AsmExtern),
    TopReturnNode,
    ForkNode,
    LiftedPhiNode, // これらは「SSA」からです
}

mod disasm_repr {
    use std::collections::hash_map::DefaultHasher as HashMapDefaultHasher;
    use std::hash::{Hash, Hasher};

    use serde_derive::{Serialize, Deserialize};

    #[derive(Debug, Clone)]
    #[non_exhaustive]
    pub enum DisassemblyLine {
        Assembly(AsmLine),
        NowInFunction(AsmFun),
        Garbage,
    }

    #[derive(Debug, Clone)]
    pub struct AsmLine {
        pub instr: AsmInstr,
        pub targeted: bool,
        pub position: AsmPosition,
    }

    #[derive(Debug, Clone)]
    pub enum AsmInstr {
        Comp(SimpleInstruction),
        Jump(AsmPosition, bool), // これは「JMP」ですか？
        FunCall(AsmFun),
        ExtCall(AsmExtern),
    }

    #[derive(Debug, Clone)]
    pub enum SimpleInstruction {
        Known {
            inp: Vec<Hashed>,
            out: Vec<Hashed>,
        },
        Unknown,
    }

    #[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct AsmPosition(u64);

    impl AsmPosition {
        pub fn new(n: u64) -> Self {
            Self(n)
        }

        pub fn inc(&self) -> Self {
            Self(self.0 + 1)
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq, Hash)]
    pub struct AsmFun(pub String);

    impl AsmFun {
        pub fn new(string: String) -> Self {
            Self(string)
        }

        pub fn init_unknown_form() -> Self {
            Self("#?".to_owned())
        }

        pub fn is_init_unknown(&self) -> bool {
            self.0 == "#?"
        }
    }

    #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct AsmExtern(Hashed);

    impl AsmExtern {
        pub fn new(s: &str) -> Self {
            Self({
                let mut hasher = HashMapDefaultHasher::new();
                s.hash(&mut hasher);
                hasher.finish()
            })
        }
    }

    type Hashed = u64;
}
