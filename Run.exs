np = 50

paths =
    File.read!("paths_to_yaml.txt")
        |> String.split("\n", trim: true)

defmodule ProcessAll do
    def init(paths, np) do
        pids =
            1..np
                |> Enum.map(
                    fn _ -> ExtractEXEs.start_link() end
                )
                |> Enum.map(
                    fn x ->
                        {:ok, pid} = x
                        pid
                    end
                )
        paths
            |> Enum.take(np)
            |> Enum.with_index
            |> Enum.each(
                fn {dt_path, ix} ->
                    GenServer.cast(
                        Enum.at(pids, ix),
                        {:proc, dt_path, [from: self()]}
                    )
                end
            )
        Enum.drop(paths, np)
    end

    def continue(paths_remaining) do
        receive do
            {:nextitem, [from: from]} ->
                GenServer.cast(
                    from,
                    {:proc, hd(paths_remaining), [from: self()]}
                )
                paths_remaining |> tl |> continue
        end
    end
end

ProcessAll.init(paths, np)
    |> ProcessAll.continue
