using MetaGraphs

using Graphs

import MsgPack

@enum NodeT begin
    ϕ
    bcompose
    extern
    top_return
end

function final_graph(fname::String)
    println("[| load.jl |] << Starting...")
    print("[| load.jl |] << Unpacking...")
    gdata = open(MsgPack.unpack, fname)
    println("done")
    print("[| load.jl |] << Fixing attributes...")
    gdata = map(gdata) do x
        attr = begin
            if isa(x[2], Dict)
                extern => (x[2] |> values |> first)
            elseif x[2] == "LiftedPhiNode"
                ϕ => UInt64(0x0)
            elseif x[2] == "CompNode"
                bcompose => UInt64(0x0)
            elseif x[2] == "TopReturnNode"
                top_return => UInt64(0x0)
            else
                error("Unknown node type ($(x[2])) found")
            end
        end
        [x[1] + 1, attr, x[3]]
    end
    println("done")
    print("[| load.jl |] << Creating graph...")
    max_node = map(x -> x[1], gdata) |> maximum
    g = MetaDiGraph(max_node)
    println("done")
    print("[| load.jl |] << Copying graph data...")
    for i in gdata
        set_prop!(g, i[1], :type, i[2])
        for into in i[3]
            add_edge!(g, into, i[1])
        end
    end
    println("done")
    print("[| load.jl |] (* VIEW raw version *) ")
    @show g
    print("[| load.jl |] << Removing self loops...")
    map(vertices(g)) do x
        rem_edge!(g, x, x)
    end
    println("done")
    print("[| load.jl |] << Removing unconnected nodes...")
    for c in connected_components(g)
        if c |> length |> ==(1)
            rem_vertex!(g, c[1])
        end
    end
    println("done")
    print("[| load.jl |] << Removing unused (lifted) ϕ nodes...")
    garbage_phi_nodes = true
    while garbage_phi_nodes
        garbage_phi_nodes = false
        for vert in vertices(g)
            if haskey(props(g, vert), :type)
                if props(g, vert)[:type].first == ϕ
                    inbound = inneighbors(g, vert)
                    if inbound |> length |> ==(1)
                        garbage_phi_nodes = true
                        item = inbound[1]
                        for outbound in outneighbors(g, vert)
                            add_edge!(g, item, outbound)
                        end
                        rem_vertex!(g, vert)
                    end
                end
            end
        end
    end
    println("done")
    println("[| load.jl |] << Graph ready!")
    print("[| load.jl |] (* VIEW ^as *) ")
    @show g
    g
end

using GraphPlot, Compose

import Cairo, Fontconfig

using Colors

function draw_graph(g::MetaDiGraph)
    types = map(vertices(g)) do vert
        if haskey(props(g, vert), :type)
            t = props(g, vert)[:type].first
            if t == ϕ "LiftedPhi"
            elseif t == bcompose "BCompose"
            elseif t == extern "Extern"
            elseif t == top_return "Return"
            else
                error("unreachable, node type not possible")
            end
        else
            "<unknown>"
        end
    end
    colordict = Dict([
        "LiftedPhi" => Colors.JULIA_LOGO_COLORS[1],
        "BCompose" => Colors.JULIA_LOGO_COLORS[2],
        "Extern" => Colors.JULIA_LOGO_COLORS[3],
        "Return" => Colors.JULIA_LOGO_COLORS[4],
        "<unknown>" => Colors.color_names["grey23"],
    ])
    colors = map(x -> colordict[x], types)
    compose_def = gplot(g, nodefillc = colors)
    draw(PNG("visualZ.png", 100cm, 100cm), compose_def)
end


const NODE_FEAT_DIMENSIONALITY = 6
const NODE_FEAT_HASHING_N_FUNCTIONS = 800

function node_feature(x::Pair{NodeT,UInt64})::Vector{Float32}
    hashdims = NODE_FEAT_DIMENSIONALITY - 2
    totaldims = NODE_FEAT_DIMENSIONALITY
    feat_empty = zeros(Float32, totaldims)
    if x == nothing
        return feat_empty
    end
    tag_part = [0.0f0, 0.0f0]
    if x.first == extern
        tag_part[1] = 1.0f0
    end
    if x.first == bcompose
        tag_part[2] = 1.0f0
    end
    hash_part = zeros(Float32, hashdims)
    if x.first == extern
        n_hash_functions = NODE_FEAT_HASHING_N_FUNCTIONS
        for f in UInt64(0x0):UInt64(n_hash_functions - 1)
            h = hash(x.second + f)
            ix = (h % (hashdims - 1)) + 1
            hash_part[ix] += 1.0f0
        end
    end
    [tag_part; hash_part]
end

get_node_data(g::MetaDiGraph) =
    vert ->
        let p = props(g, vert)
            if haskey(p, :type)
                p[:type]
            else
                nothing
            end
        end

function into_coo_fmt(g::MetaDiGraph)
    fmt_1 = Vector{UInt16}()
    fmt_2 = Vector{UInt16}()
    for i in vertices(g)
        for j in inneighbors(g, i)
            push!(fmt_1, i)
            push!(fmt_2, j)
        end
    end
    (fmt_1, fmt_2)
end

function translate_fmt(g::MetaDiGraph, is_malw::Bool)
    print("<sub:rm:")
    top_rnode = findfirst(vertices(g)) do vert
        haskey(props(g, vert), :type) && (props(g, vert)[:type].first == top_return)
    end
    if top_rnode != nothing
    	rem_vertex!(g, top_rnode)
    end
    if length(ARGS) >= 3 && ARGS[3] == "alsoDRAW"
        print("[draw...")
        draw_graph(g)
        print("ok]")
    end
    print("ok><node:")
    node_features = map(node_feature ∘ get_node_data(g), vertices(g))
    print("ok><coo:")
    coo_fmt = into_coo_fmt(g::MetaDiGraph)
    print("ok>")
    Dict(
        "xval" => is_malw,
        "node" => node_features,
        "coo1" => coo_fmt[1],
        "coo2" => coo_fmt[2],
    )
end

using JLD2

using FileIO

using UUIDs

const len = length

function main_given(asm_fname::String)
    println("[| load.jl |] << Building graph (Rust, `cargo run`) ....")
    run(`cargo run $asm_fname`)
    println("[| load.jl |] ^= done")
    msgpack_fname = asm_fname * ".generated.ASM_COMPOSE_GRAPH.mp"
    g = final_graph(msgpack_fname)
    print("[| load.jl |] << Correcting graph...")
    g2 = translate_fmt(g, occursin("malw", ARGS[1]))
    println("...done")
    if len(g2["coo1"]) <= 5
	    println("!!!!!!!!! failed, not enough edges (something went wrong)")
	    error("Not enough edges (REQ `ne(g) > 5`). This graph will not be saved. ")
	    return
    end
    print("[| load.jl |] << Saving MsgPack...")
    mp2_fname = "$(ARGS[4])/z_$(uuid1() |> string).ok.FINAL.DATA.mp"
    open(mp2_fname, create=true, write=true) do fstream
        MsgPack.pack(fstream, g2)
    end
    println("done")
    println("[| load.jl |] !! [INFO] nv=$(len(g2["node"])), ne=$(len(g2["coo1"])), [END]")
    #=
    print("[| load.jl |] << Saving original graph [fmt: :JLD2, as_backup: true]... ")
    save(mp2_fname * ".jlBKUP_ORIG.jld2", Dict("graph_result" => g2))
    println("[| load.jl |] @@ WORK_COMPLETED")
    println("[| load.jl |] @@ Set mp2_fname := $(mp2_fname)")
    =#
end

println("[| load.jl |] @@ STARTED")
println("[| load.jl |] @@ Recieved ARGS := $(ARGS)")
main_given(ARGS[2])
println("[| load.jl |] @@ END (time report below)")
