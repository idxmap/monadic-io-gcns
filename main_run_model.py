import os
import os.path as osp

import torch
from torch_geometric.data import Dataset, Data

import msgpack

import uuid

import sys

from scipy.sparse import coo_matrix

import networkx as nx
import networkx.algorithms as nxalgs

import numpy

# -- from threading import Thread
from multiprocessing import Process

class BaselineDataset(Dataset):
    def __init__(self, root, transform=None, pre_transform=None, pre_filter=None):
        super().__init__(
            root, transform, pre_transform, pre_filter
        )

    @property
    def raw_file_names(self):
        return [
            *os.listdir(self.z_malw_srcdir),
            *os.listdir(self.z_safe_srcdir),
        ]

    @property
    def processed_file_names(self):
        return []

    @property
    def z_malw_srcdir(self):
        return f'{os.environ["HOME"]}/Documents/LTP_2022/forGCN/Zfinal_srcdir_malw'

    @property
    def z_safe_srcdir(self):
        return f'{os.environ["HOME"]}/Documents/LTP_2022/forGCN/Zfinal_srcdir_safe'

    def download(self):
        # Download to `self.raw_dir`.
        os.system(f'cp {self.z_malw_srcdir}/*.DATA.mp {self.raw_dir}')
        os.system(f'cp {self.z_safe_srcdir}/*.DATA.mp {self.raw_dir}')

    def load_mp_raw_data(self, raw_path):
        dict_form = None
        with open(raw_path, 'rb') as f:
            dict_form = msgpack.unpackb(f.read())
        edge_index = torch.tensor([
            list(map(lambda x: x - 1, dict_form['coo1'])),
            list(map(lambda x: x - 1, dict_form['coo2'])),
        ]).to(torch.int64)
        if dict_form['xval']:
            y = torch.tensor(1).to(torch.long)
        else:
            y = torch.tensor(0).to(torch.long)
        return (dict_form['node'], edge_index, y)

    def call_graph(self, G):
        """
        For some reason, in NetworkX, G.nodes is an iterator of node identifiers,
        but when you index it, you get the attributes of the node at that index.
        It's like a dictionary but when you use it as an iterator it only gives
        you the keys which is sort of weird but kind of makes sense because
        hash sets are just hash maps with values of the unit type. Also, this is
        how the `in` operator works (on keys).
        """
        G2 = nx.DiGraph()
        Gcomp = nx.DiGraph() # We only want paths through composition nodes to count
        g_ix = 0
        gcomp_ix = 0
        gcomp_map = {} # Map[G.Node, Gcomp.Node] (path dependent types!)
        g2_map = {}    # Map[G2.Node, G.Node]
        for i in G.nodes:
            # This should be 0.0 or 1.0, but floating-point equality is weird
            # and I don't want to break something.
            # The point of this is to ignore Kleisli composition nodes
            if (G.nodes[i])['dt'][2] >= 0.5:
                #[DBG2] print('.', end='')
                Gcomp.add_node(
                    gcomp_ix,
                )
                gcomp_map[i] = gcomp_ix
                gcomp_ix += 1
                continue
            # Add to function nodes
            G2.add_node(
                g_ix,
                dt = (
                    (G.nodes[i])['dt']
                ),
            )
            """
            ^ Normally, you would write `**( (G.nodes[i] )`, but splatting kwargs
            is really slow.
            """

            g2_map[g_ix] = i

            #[DBG2] print(i % 10, end='')
            g_ix += 1

        # Add edges
        # (this shouldn't have been done in the previous step bc only now are there no composition nodes)
        for i in G2.nodes:
            for j in G2.nodes:
                g_src_ix = g2_map[i]
                g_dst_ix = g2_map[j]
                src_group = \
                    map(lambda x: x[1], G.out_edges(g_src_ix))
                dst_group = \
                    map(lambda x: x[0], G.in_edges(g_dst_ix))
                for gsrc in src_group:
                    if gsrc not in gcomp_map:
                        continue
                    csrc = gcomp_map[gsrc]
                    for gdst in dst_group:
                        if gdst not in gcomp_map:
                            continue
                        cdst = gcomp_map[gdst]
                        if nxalgs.shortest_paths.generic.has_path(Gcomp, csrc, cdst):
                            #[DBG2] print('*', end='')
                            G2.add_edge(i, j)
        return G2

    def correct_node_feat(self, node_feat_list):
        node_feat = node_feat_list
        return [
            node_feat[0],
            *node_feat[2:],
        ]

    def process_one(self, raw_path, idx):
        # Read data from `raw_path`.
        (node_orig, edge_index_orig, y) = self.load_mp_raw_data(raw_path)
        G = nx.DiGraph()
        G.add_nodes_from(
            (
                (
                    idx,
                    { 'dt': i },
                ) \
                for (idx, i) in enumerate(node_orig)
            ),
        )
        G.add_edges_from(
            (
                (
                    i[0].item(),
                    i[1].item(),
                ) \
                for i in edge_index_orig.t()
            ),
        )
        G2 = self.call_graph(G)
        coo_fmt_cg = nx.convert_matrix.to_scipy_sparse_matrix(G2, format='coo')
        edge_index = torch.tensor(
            numpy.array([
                coo_fmt_cg.row,
                coo_fmt_cg.col,
            ])
        )
        x = torch.tensor(
            [
                self.correct_node_feat((G2.nodes[i])['dt']) for i in G2.nodes
            ],
        )
        data = Data(
            x=x,
            edge_index=edge_index,
            y=y,
        )

        if self.pre_filter is not None and not self.pre_filter(data):
            return

        if self.pre_transform is not None:
            data = self.pre_transform(data)

        torch.save(data, osp.join(self.processed_dir, f'z_PyG_data_{idx}.pt'))

    def process(self):
        idx = 0
        threads = []
        for raw_path in self.raw_paths:
            t = Process(target=self.process_one, args=(raw_path, idx))
            idx += 1
        for t in threads:
            t.start()
        #for t in threads:
        #    t.join()
        input("waiting for user input...")

    def len(self):
        if self.pre_filter is not None:
            raise NotImplementedError
        else:
            return len(self.raw_file_names)

    def get(self, idx):
        data = torch.load(osp.join(self.processed_dir, f'z_PyG_data_{idx}.pt'))
        data.x = data.x.to(torch.float32)
        data.edge_index = data.edge_index.to(torch.long)
        data.num_nodes = data.x.size(0)
        return data

class ProjectDataset(Dataset):
    def __init__(self, root, transform=None, pre_transform=None, pre_filter=None):
        super().__init__(
            root, transform, pre_transform, pre_filter
        )

    @property
    def raw_file_names(self):
        return [
            *os.listdir(self.z_malw_srcdir),
            *os.listdir(self.z_safe_srcdir),
        ]

    @property
    def processed_file_names(self):
        return []

    @property
    def z_malw_srcdir(self):
        return f'{os.environ["HOME"]}/Documents/LTP_2022/forGCN/Zfinal_srcdir_malw'

    @property
    def z_safe_srcdir(self):
        return f'{os.environ["HOME"]}/Documents/LTP_2022/forGCN/Zfinal_srcdir_safe'

    def download(self):
        # Download to `self.raw_dir`.
        os.system(f'cp {self.z_malw_srcdir}/*.DATA.mp {self.raw_dir}')
        os.system(f'cp {self.z_safe_srcdir}/*.DATA.mp {self.raw_dir}')

    def load_raw_data(self, raw_path):
        dict_form = None
        with open(raw_path, 'rb') as f:
            dict_form = msgpack.unpackb(f.read())
        x = torch.tensor(
            dict_form['node']
        ).to(torch.float32)
        edge_index = torch.tensor([
            list(map(lambda x: x - 1, dict_form['coo1'])),
            list(map(lambda x: x - 1, dict_form['coo2'])),
        ]).to(torch.int64)
        if dict_form['xval']:
            y = torch.tensor(1).to(torch.long)
        else:
            y = torch.tensor(0).to(torch.long)
        return Data(
            x=x,
            edge_index=edge_index,
            y=y,
        )

    def process(self):
        idx = 0
        for raw_path in self.raw_paths:
            # Read data from `raw_path`.
            data = self.load_raw_data(raw_path)

            if self.pre_filter is not None and not self.pre_filter(data):
                continue

            if self.pre_transform is not None:
                data = self.pre_transform(data)

            torch.save(data, osp.join(self.processed_dir, f'z_PyG_data_{idx}.pt'))
            idx += 1

    def len(self):
        if self.pre_filter is not None:
            raise NotImplementedError
        else:
            return len(self.raw_file_names)

    def get(self, idx):
        data = torch.load(osp.join(self.processed_dir, f'z_PyG_data_{idx}.pt'))
        data.num_nodes = data.x.size(0)
        return data

is_m_baseline = sys.argv[1] == 'm_baseline'

import time

if is_m_baseline:
    print('[py-RECV] sys.argv[1] == m_baseline')
    time.sleep(1)

dataset = (BaselineDataset if is_m_baseline else ProjectDataset)(
    root = f'/tmp/gcnproject--{uuid.uuid4()}/active-data/',
)

torch.manual_seed(1237)
dataset = dataset.shuffle()

max_idx = len(dataset)

train_test_split = 0.3

train_test_index = int(max_idx * train_test_split)

val_test_split = 0.4

withheld_max = int(max_idx * val_test_split)

is_validation = True

train_dataset = dataset[:train_test_index]
if is_validation:
    test_dataset = dataset[train_test_index:withheld_max]
else:
    test_dataset = dataset[withheld_max:]

print(f'Number of training graphs: {len(train_dataset)}')
print(f'Number of test graphs: {len(test_dataset)}')

from torch_geometric.loader import DataLoader

batch_size = 110

train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

for step, data in enumerate(train_loader):
    print(f'Step {step + 1}:')
    print('=======')
    print(f'Number of graphs in the current batch: {data.num_graphs}')
    print(data)
    print()

from torch.nn import Linear, Softmax
import torch.nn.functional as F
from torch_geometric.nn import GCNConv
from torch_geometric.nn import global_mean_pool, global_max_pool

class GCN(torch.nn.Module):
    def __init__(self, hidden_channels, hidden_channels_linear):
        super(GCN, self).__init__()
        torch.manual_seed(1237)
        self.conv1 = GCNConv(dataset.num_node_features, hidden_channels)
        self.conv2 = GCNConv(hidden_channels, hidden_channels)
        self.conv3 = GCNConv(hidden_channels, hidden_channels_linear)
        #- self.conv1 = GCNConv(dataset.num_node_features, hidden_channels_linear)
        self.lin_hidden1 = Linear(hidden_channels_linear, hidden_channels_linear)
        self.lin_hidden2 = Linear(hidden_channels_linear, hidden_channels_linear)
        self.lin_final = Linear(hidden_channels_linear, 2)
        self.softmax = Softmax(dim=1)

    def forward(self, x, edge_index, batch):
        # 1. Obtain node embeddings
        x = self.conv1(x, edge_index)
        #print('x = ', end=''); print(x)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = x.relu()
        x = self.conv3(x, edge_index)

        # 2. Readout layer
        x = global_mean_pool(x, batch)  # [batch_size, hidden_channels]

        # 3. Apply a final classifier
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin_hidden1(x)

        x = x.relu()

        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin_hidden2(x)

        x = x.relu()

        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin_final(x)

        #- x = self.softmax(x)

        #print('q = ', end=''); print(x)

        return x

n_hidden_channels = 16

n_hidden_channels_linear = 16

model = GCN(hidden_channels=n_hidden_channels, hidden_channels_linear=n_hidden_channels_linear)
print(model)

model = GCN(hidden_channels=n_hidden_channels, hidden_channels_linear=n_hidden_channels_linear)
optimizer = torch.optim.Adam(
    model.parameters(),
    lr = 0.0008,
)
criterion = torch.nn.CrossEntropyLoss()

def train():
    model.train()

    for data in train_loader:  # Iterate in batches over the training dataset.
        #- [ i.retain_grad() for i in model.parameters() ]
        out = model(data.x, data.edge_index, data.batch)  # Perform a single forward pass.
        loss = criterion(out, data.y)  # Compute the loss.
        print(f'Loss: {loss:.6f}')
        loss.backward()  # Derive gradients.
        optimizer.step()  # Update parameters based on gradients.
        optimizer.zero_grad()  # Clear gradients.

def test(loader):
     model.eval()

     correct = 0
     for data in loader:  # Iterate in batches over the training/test dataset.
         out = model(data.x, data.edge_index, data.batch)
         # print(f'out = {out}')
         pred = out.argmax(dim=1).to(torch.long)  # Use the class with highest probability.
         # print(f'pred = {pred}')
         truth = data.y.to(torch.long)
         # print(f'truth = {truth}')
         # print((pred, truth))
         correct += int((pred == truth).sum())  # Check against ground-truth labels.
     return correct / len(loader.dataset)  # Derive ratio of correct predictions.

n_epochs = 70

import itertools

#- for epoch in range(1, n_epochs):
for epoch in itertools.count(1):
    train()
    train_acc = test(train_loader)
    test_acc = test(test_loader)
    print(f'Epoch: {epoch:03d}, Train Acc: {train_acc:.4f}, Test Acc: {test_acc:.10f}')
